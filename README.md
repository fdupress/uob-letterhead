# UoB-Letterhead

A $`\LaTeX`$ template for
[styleguide](https://www.bristol.ac.uk/style-guides/brand-identity/)-compliant(-ish)
University of Bristol letterhead. It attempts to stick close to the [letterhead
template](https://uob.sharepoint.com/:w:/r/sites/marketing/_layouts/15/Doc.aspx?sourcedoc=%7B8B765B9A-105E-41EC-9B88-B2D94D9C5AFE%7D&file=Letterhead%20word%20doc.docx&action=default&mobileredirect=true).
(Link requires UoB authentication.)

## Dependencies

Relies on:
- [KOMA-Script](https://komascript.de/) (>= 3.08); (read the documentation for
  KOMA-Script's `scrlttr2` class to figure things out further)
- graphicx;
- xcolor;
- inter;
- fontenc;
- babel; and
- hyperref.

## Fonts

The brand guidelines list specific fonts for use in official communications. I
suspect they are not meant to cover private letters, but who knows when one
might be called upon to write an open letter on the University's behalf?

### Fonts for body copy

Following brand guidelines, text is typeset using
[Inter](https://https://rsms.me/inter/), which is available under the [SIL open
font license](https://raw.githubusercontent.com/rsms/inter/v4.0/LICENSE.txt).

Brand guidelines list Arial as a possible alternative font to use for copy.
This is widely
[available](https://sourceforge.net/projects/corefonts/files/the%20fonts/final/arial32.exe/download)
but you should make sure you
[acquire](https://www.myfonts.com/fonts/mti/arial/) the appropriate license for
your use case.

### Fonts for large headers

For “large headers”, brand guidelines mandate the use of [FS
Rufus](https://www.fontsmith.com/fonts/fs-rufus) or, if needed, [Rockwell
Regular](https://www.myfonts.com/fonts/mti/rockwell/).

I can't think of a reason one might need to typeset large headers on
letterhead, but hey! You do you. But then you figure out how to get those
fonts, acquire the appropriate license and get $`\LaTeX`$ to play well with them.

## Logo

An SVG version of the [UoB
logo](https://gitlab.com/fdupress/uob-letterhead/-/blob/master/logo-src/bristol.svg)
is included in this repo.

The letterhead template does not use it directly. We instead generate a PDF and
a wrapper using the following command. (A result is included in the [`logo`
folder](https://gitlab.com/fdupress/uob-letterhead/-/blob/master/logo/) for
ease of use.)

```bash
inkscape -D logo-src/bristol.svg  -o logo/bristol.pdf --export-latex
```

If you fiddle with the SVG logo, you can always produce the PDF and wrapper
again using the same command. (This requires a version of `inkscape` from after
July 2020.)

## Sample

- [letterhead.tex](https://gitlab.com/fdupress/uob-letterhead/-/blob/master/letterhead.tex)
  produced the sample letters in
  [letterhead.pdf](https://gitlab.com/fdupress/uob-letterhead/-/blob/master/sample/letterhead.pdf)
  and is used to produce the same
  [dynamically](https://gitlab.com/fdupress/uob-letterhead/-/jobs/artifacts/master/raw/letterhead.pdf?job=typeset)
  (this may reflect some changes made since the sample was uploaded).

## Licensing and Copyright

The University of Bristol logo is owned and copyrighted by the University of
Bristol. Its distribution here does not imply a licence to the end user.

To the extent that copyright applies to templates, the rest of the template is
distributed under the terms of CC0 (see
[LICENSE](https://gitlab.com/fdupress/uob-letterhead/-/blob/master/LICENSE));
feel free to use, modify and redistribute.
